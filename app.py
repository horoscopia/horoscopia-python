
import os
from os.path import join, dirname
from dotenv import load_dotenv

from modules.command_system import CommandSystem
from modules.networker import Networker
from modules.horoscope import Horoscope

# load environment variables
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
user_id = os.getenv('ASTROLOGY_API_USER_ID')
key = os.getenv('ASTROLOGY_API_KEY')

# provide api information to a new networker
networker = Networker(user_id, key)

# start up command-line system
command_system = CommandSystem(networker)
command_system.initiate()

# predictions = networker.retrieve_predictions(sign)
# horoscope.add_predictions(predictions)
# horoscope.log_predictions()



