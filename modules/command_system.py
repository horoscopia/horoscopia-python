from modules.console_tools import color_codes, print_divider, center_text, apply_color_to_string, log_error, log_with_embedded_code
from modules.horoscope import determine_sign, Horoscope
from modules.networker import Networker

class CommandSystem:

    def __init__(self, networker):
        self._networker = networker
        self._exit = False
        self._horoscope = Horoscope()
        return

    # welcomes the user
    def __welcome(self):
        # styles the header and centers it
        header = 'welcome to horoscopia | python-edition'
        header = center_text(header)
        header = apply_color_to_string(header, color_codes.OKBLUE)

        print('\n')
        print_divider()
        print_divider()
        print(header)
        print_divider()
        print_divider()
        print('')

    # lists all available commands to the user
    def __list_commands(self):
        # styles the header and centers it
        header = 'commands'
        header = center_text(header)
        header = apply_color_to_string(header, color_codes.OKBLUE)

        print_divider()
        print(header)
        print_divider()
        # weird marks are to indicate code indentations for the printed output to highlight
        log_with_embedded_code('`determine-sign ~[~numbered_month~]~:~[~numbered_day~]~`     |    determine your zodiac sign based on birthday')
        log_with_embedded_code('`predict`                                            |    predict horoscope based on determined zodiac sign')
        log_with_embedded_code('`exit`                                               |    end program')
        print_divider()

    # fail safe if the user types a command that doesn't exist
    def __execute_default(self, command):
        log_error(f'{apply_color_to_string(command, color_codes.OKGREEN)} is not a command')

    # terminates program
    def __end_program(self):
        self._exit = True

    # takes the command and removes whitespace at beginning and end
    def __clean_command(self, command):
        while command.startswith(' '):
            command = command[1:len(command)]
        while command.endswith(' '):
            command = command[0:len(command)-1]
        return command

    # displays determined zodiac sign to the user
    def __output_sign(self, sign):
        output = ''

        # styles output
        for i in sign:
            output += f' {i} '
        output = center_text(output)
        output = apply_color_to_string(output, color_codes.WARNING)

        print_divider()
        print(output)
        print_divider()

    # logic for the user's input
    def __execute_command(self, command):
        command = self.__clean_command(command)
        base_command = command.split(' ')[0]

        # help command lists available commands
        if command == 'help':
            self.__list_commands()
        # exit command terminates the program
        elif command == 'exit':
            self.__end_program()
        # predict command logs retrieved predictions in the horoscope object
        elif command == 'predict':
            sign = self._horoscope.get_sign()

            if 'unchosen' in sign:
                log_error(f'we don\'t know your zodiac sign yet. please run {apply_color_to_string("determine-sign", color_codes.OKGREEN)} first')
            else:
                self.__output_sign(sign)
                self._horoscope.log_predictions()

        # determine-sign command determines the user's zodiac sign
        # based on the users given birthday
        elif base_command == 'determine-sign':
            misformatted = False
            month = -1
            day = -1

            # trims base_command to narrow down the provided details
            if command.startswith('determine-sign '):
                command = command.replace('determine-sign ', '')
            else:
                misformatted = True
            
            # splits month from day if : was properly provided
            if ':' in command:
                split = command.split(':')

                # checks for extra junk that wouldn't be needed
                if len(split) != 2:
                    misformatted = True

                # tries to parse strings to int and set the month and day
                try: 
                    month = int(split[0])
                    day = int(split[1])
                except ValueError:
                    misformatted = True

            else:
                misformatted = True

            # if any of the checks failed, an improper formatting error is logged to the user
            if misformatted:
                log_error(f'improper formatting for command {apply_color_to_string("determine-sign", color_codes.OKGREEN)}. type {apply_color_to_string("help", color_codes.OKGREEN)} for details')
            # if everything was typed correctly, the sign is determined
            else:
                sign = determine_sign(month, day)

                # if everything was typed correctly, but the provided numbers didn't represent
                # a valid date, an error is logged to the user
                if('invalid-input' in sign):
                    log_error(f'the date you provided doesn\'t exist')
                # if sign comes back valid it's output and saved in memory
                else:
                    self.__output_sign(sign)

                    # retrieves predictions based on given zodiac sign
                    predictions = self._networker.retrieve_predictions(sign)

                    # creates a new horoscope and stores the data for when the user calls the
                    # predict command
                    self._horoscope = Horoscope()
                    self._horoscope.set_sign(sign)
                    self._horoscope.add_predictions(predictions) 

        else:
            self.__execute_default(command)

    # function waits for user input and then executes the command
    def __await_command(self):
        # prompts user input
        command = input(f'\nenter command (type {apply_color_to_string("help", color_codes.OKGREEN)} for options): {color_codes.OKGREEN}')
        print(f' {color_codes.ENDC}')

        # executes command
        self.__execute_command(command)

    # iniates command system cycle
    def initiate(self):
        # welcomes user
        self.__welcome()

        # cycles for input until prompted to exit
        while not self._exit:
            self.__await_command()