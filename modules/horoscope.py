import math
from tabulate import tabulate
from modules.console_tools import color_codes, apply_color_to_string, log_error

# the amount of days in every month chronologically ordered
dayCounts = [ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ]

# takes the month and day and makes sure the date actually
# exists
def verify_date(month, day):
    if month > 12 or month < 1:
        return False
    else:
        return dayCounts[month - 1] >= day

# takes the month and day and determines the zodiac sign
# returns 'invalid-input' if date doesn't exist
def determine_sign(month, day):
    if verify_date(month, day) == False:
        return 'invalid-input'

    if (month == 3 and day >= 21) or (month == 4 and day <= 19):
        return 'aries'
    elif (month == 4 and day >= 20) or (month == 5 and day <= 20):
        return 'taurus'
    elif (month == 5 and day >= 21) or (month == 6 and day <= 20):
        return 'gemini'
    elif (month == 6 and day >= 21) or (month == 7 and day <= 22):
        return 'cancer'
    elif (month == 7 and day >= 23) or (month == 8 and day <= 22):
        return 'leo'
    elif (month == 8 and day >= 23) or (month == 9 and day <= 22):
        return 'virgo'
    elif (month == 9 and day >= 23) or (month == 10 and day <= 22):
        return 'libra'
    elif (month == 10 and day >= 23) or (month == 11 and day <= 21):
        return 'scorpio'
    elif (month == 11 and day >= 22) or (month == 12 and day <= 21):
        return 'sagittarius'
    elif (month == 12 and day >= 22) or (month == 1 and day <= 19):
        return 'capricorn'
    elif (month == 1 and day >= 20) or (month == 2 and day <= 18):
        return 'aquarius'
    elif (month == 2 and day >= 19) or (month == 3 and day <= 20):
        return 'pisces'
    else:
        return 'invalid-input'

# class used to represent all of the users horoscope data
class Horoscope:

    def __init__(self, sign = 'unchosen'):
        self._sign = sign
        self._predictions = []

    def get_sign(self):
        return self._sign

    def set_sign(self, sign):
        self._sign = sign

    # adds prediction to be logged out to the user
    def add_prediction(self, subject, content):
        # ensures content is actually text
        if type(content) is str:
            prediction = { 'subject': subject, 'content': content }
            
            self._predictions.append(prediction)
        else:
            log_error('prediction content wasn\'t a string and was skipped')


    # adds a list of predictions to be logged to the user
    def add_predictions(self, predictions):
        # ensures predictions are properly formatted
        if type(predictions) is dict:
            for key in predictions.keys():
                self.add_prediction(key, predictions[key])
        else:
            log_error('prediction to add was inproperly formatted')


    # logs all predictions stored in this instance to the user
    def log_predictions(self):

        # used to split content onto multiple lines
        contentCharactersPerLine = 95

        # initialize a table to log in table format to user
        table = [['', '']]

        # loops through predictions adding the info to the table
        for prediction in self._predictions:
            subject = prediction['subject']
            content = prediction['content']
            contentLength = len(content)

            # determines into how many lines the content should be split up
            linesOfContent = contentLength / contentCharactersPerLine
            linesOfContent = math.ceil(linesOfContent)
            
            # the start and end indexes used to create line breaks for the
            # content
            start = 0
            end = contentCharactersPerLine 

            # this logic splits the content onto multiple lines in the output
            # in order to create a better presentation for the user
            while start < contentLength:
                firstColumn = ''
                secondColumn = ''

                # this logic ensures that the line break happens on a space
                # and not in between a word
                if end > len(content):
                    end = len(content)
                else:
                    while content[end-1] != ' ':
                        end -= 1
                
                # sets color of the subject and sets it as the first column,
                # given that there wasn't a line break
                if start == 0:
                    firstColumn = apply_color_to_string(subject, color_codes.HEADER)

                # sets and colors the second column to the current line of content
                secondColumn = apply_color_to_string(content[start:end], color_codes.OKCYAN)
                toAppend = [firstColumn, secondColumn]

                # appends results to the table
                table.append(toAppend)

                # updates the start and end index for grabbing the next line of content
                start = end
                end += contentCharactersPerLine
                
            table.append(['', ''])

        # prints the generated table in table format to the user
        print('')
        print(tabulate(table))
        print('')
