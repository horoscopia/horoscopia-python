import math

# color codes can be added to text to color output
class color_codes:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[36m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# this is the width that the console output is centered and formatted around
line_length = 110

# prints a divider to the console
def print_divider():
    global line_length

    print('-' * line_length)

# centers text based on line_length to create a nice header in the console
def center_text(text):
    global line_length
    text_length = len(text)

    # first makes sure the text isn't longer than the line_length
    if text_length < line_length:
        # this padding stuff is just to find out how many spaces to put in the text
        # to center it
        total_padding = line_length - text_length
        left_padding = int((total_padding / 2) + (total_padding % 2))
        # creates spaces and puts it to the left of text to center it
        left_padding_text = ' ' * left_padding
        text = left_padding_text + text
    return text


# takes a string and colors it
def apply_color_to_string(string, color_code):
    return f'{color_code}{string}{color_codes.ENDC}'

# logs an error message in the given format
def log_error(error):
    print(f'{color_codes.FAIL}error:{color_codes.ENDC}\t{error}')

# text with code inside is styled to indicate this
def log_with_embedded_code(text):

    # text inside graves are commands and are colored green
    while '`' in text:
        text = text.replace('`', color_codes.OKGREEN, 1)

        if '`' in text:
            text = text.replace('`', color_codes.ENDC, 1)
        else:
            text += color_codes.ENDC
    # text inside tildas are used to indicate variable commands
    while '~' in text:
        text = text.replace('~', color_codes.OKCYAN, 1)

        if '~' in text:
            text = text.replace('~', color_codes.OKGREEN, 1)
        else:
            text += color_codes.OKGREEN

    print(text)
