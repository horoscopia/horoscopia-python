
from modules.astrology_api_sdk import VRClient
from modules.console_tools import log_error

class Networker:

    # takes user_id and key needed to retrieve predictions from the api
    def __init__(self, user_id, key):
        self.user_id = user_id
        self.key = key
        # this is used to call the api for predictions
        self.astrology_api_client_instance = VRClient(user_id, key)
        

    # function retrieves predictions based on sign provided
    def retrieve_predictions(self, sign):
        # makes a call to the api
        response = self.astrology_api_client_instance.call(f'sun_sign_prediction/daily/{sign}/', '', '', '', '', '', '', '', '5.5')

        # if the response comes back sucessful, the data is converted to json and returned
        if response.status_code == 200:
            json = response.json()

            # if response was successful but data wasn't provided, this indicates a bad key or user id
            # this handles that
            if 'prediction' in json:
                predictions = response.json()['prediction']
                return predictions
            else:
                log_error('something is wrong with your user id or api key')
                return {}

        # else the failed call is logged to the user
        else:
            log_error('failed retrieving predictions from the server')
            return {}

    