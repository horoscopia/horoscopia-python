# horoscopia | python-edition | version-1

## description

>Who needs professional counseling when you can just look at the stars! By providing your birthday, we'll determine your zodiac sign and provide you with the insight and guidance of the universe.
>
>This project is a command-line Python app that utilizes the API [AstrologyAPI](https://www.astrologyapi.com/) to provide predictions based on your birthday.

## setup 
- clone project
- install **python** if not installed:   https://www.python.org/downloads/
- run `pip install -r requirements.txt` in project directory to install dependencies
- obtain a premium account (14 day trial) from astrologyapi:  https://www.astrologyapi.com/
- create `.env` in project directory containing this:
>>> `ASTROLOGY_API_USER_ID=[YOUR_USER_ID]` 
>>>
>>> `ASTROLOGY_API_KEY=[YOUR_KEY]`

## usage
* open console and keep it wide enough for at 120-150 columns of characters
* run `python app.py` or `python3 app.py` in project directory to run app